package com.sc.demo2.common.config;

import com.sc.common.enums.ApplicationEnum;
import com.sc.common.interceptors.ContextHandlerDefaultInterceptor;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.*;


/**
 * Created by wust on 2019/2/18.
 */
@SpringBootConfiguration
public class WebMvcConfig implements WebMvcConfigurer {


    @Bean
    public ContextHandlerDefaultInterceptor contextHandlerDefaultInterceptor() {
        return new ContextHandlerDefaultInterceptor();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("*")
                .maxAge(3600)
                .allowedHeaders("X-Requested-With", "Content-Type", ApplicationEnum.X_AUTH_TOKEN.getStringValue());
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(contextHandlerDefaultInterceptor()); // 业务上下文配置
    }



    /**
     * 添加静态资源--过滤swagger-api (开源的在线API文档)
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.
                addResourceHandler("/swagger-ui/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/")
                .resourceChain(false);

    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/swagger-ui/")
                .setViewName("forward:/swagger-ui/index.html");
    }
}

/**
 * Created by wust on 2019-11-27 13:57:02
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.demo2.core.api.feign;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.annotations.FeignApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.demo2.api.TestService;
import com.sc.demo2.core.dao.TestMapper;
import com.sc.demo2.entity.test.Test2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: wust
 * @date: Created in 2019-11-27 13:57:02
 * @description:
 *
 */
@FeignApi
@RestController
public class TestFeignApi implements TestService {
    @Autowired
    private TestMapper demoMapper;


   @Override
    @RequestMapping(value = SELECT,method= RequestMethod.POST)
    public WebResponseDto select(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody Test2 search) {
        WebResponseDto responseDto = new WebResponseDto();
        List<Test2> list = demoMapper.select(search);
        if(CollectionUtil.isNotEmpty(list)){
            responseDto.setObj(JSONObject.toJSONString(list));
        }
        return  responseDto;
    }
}

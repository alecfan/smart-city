package com.sc.demo1.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.demo1.entity.test.Test1;

/**
 * @author: wust
 * @date: 2020-07-04 13:40:17
 * @description:
 */
public interface TestMapper extends IBaseMapper<Test1>{
}
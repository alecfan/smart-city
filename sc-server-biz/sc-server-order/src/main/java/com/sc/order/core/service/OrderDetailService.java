package com.sc.order.core.service;

import com.sc.common.service.BaseService;
import com.sc.order.entity.detail.OrderDetail;

/**
 * @author: wust
 * @date: 2020-07-15 14:16:52
 * @description:
 */
public interface OrderDetailService extends BaseService<OrderDetail>{
}

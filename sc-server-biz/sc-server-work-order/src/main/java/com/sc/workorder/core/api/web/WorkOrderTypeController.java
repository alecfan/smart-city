package com.sc.workorder.core.api.web;


import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.common.dto.PageDto;
import com.sc.workorder.entity.WoWorkOrderType;
import com.sc.workorder.entity.WoWorkOrderTypeSearch;
import com.sc.workorder.core.bo.WorkOrderTypeBo;
import com.sc.workorder.core.service.WoWorkOrderTypeService;
import com.sc.common.annotations.WebApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.context.DefaultBusinessContext;
import java.util.Date;

/**
 * @author: wust
 * @date: 2020-04-02 13:06:33
 * @description:
 */
@WebApi
@RequestMapping("/web/WorkOrderTypeController")
@RestController
public class WorkOrderTypeController {
    @Autowired
    private WoWorkOrderTypeService woWorkOrderTypeServiceImpl;

    @Autowired
    private WorkOrderTypeBo workOrderTypeBo;

   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody WoWorkOrderTypeSearch search){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       search.setProjectId(ctx.getProjectId());
       search.setCompanyId(ctx.getBranchCompanyId());

       PageDto pageDto = search.getPageDto();
       Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());
       List<WoWorkOrderType> list =  woWorkOrderTypeServiceImpl.select(search);

       BeanUtils.copyProperties(page,pageDto);
       responseDto.setPage(pageDto);
       responseDto.setLstDto(list);
       return responseDto;
   }



   @RequestMapping(value = "",method = RequestMethod.POST)
   public WebResponseDto create(@RequestBody  WoWorkOrderType entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       WoWorkOrderType woWorkOrderTypeSearch = new WoWorkOrderType();
       woWorkOrderTypeSearch.setName(entity.getName());
       woWorkOrderTypeSearch.setCompanyId(ctx.getBranchCompanyId());
       WoWorkOrderType woWorkOrderType = woWorkOrderTypeServiceImpl.selectOne(woWorkOrderTypeSearch);
       if(woWorkOrderType != null){
           responseDto.setFlag(WebResponseDto.INFO_WARNING);
           responseDto.setMessage("该类型已经存在！");
           return responseDto;
       }

       entity.setProjectId(ctx.getProjectId());
       entity.setCompanyId(ctx.getBranchCompanyId());
       entity.setCreaterId(ctx.getAccountId());
       entity.setCreaterName(ctx.getAccountName());
       entity.setCreateTime(new Date());
       woWorkOrderTypeServiceImpl.insert(entity);
       return responseDto;
   }


   @RequestMapping(value = "",method = RequestMethod.PUT)
   public WebResponseDto update(@RequestBody  WoWorkOrderType entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setModifyId(ctx.getAccountId());
       entity.setModifyName(ctx.getAccountName());
       entity.setModifyTime(new Date());
       woWorkOrderTypeServiceImpl.updateByPrimaryKeySelective(entity);
       return responseDto;
   }


   @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
   public WebResponseDto delete(@PathVariable Long id){
       WebResponseDto responseDto = new WebResponseDto();
       woWorkOrderTypeServiceImpl.deleteByPrimaryKey(id);
       return responseDto;
   }



    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public WebResponseDto detail(@PathVariable Long id){
        WebResponseDto responseDto = new WebResponseDto();
        WoWorkOrderType entity = woWorkOrderTypeServiceImpl.selectByPrimaryKey(id);
        if(entity != null){
            responseDto.setObj(entity);
        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("没有该记录！");
            return responseDto;
        }
        return responseDto;
    }


    @RequestMapping(value = "/buildCascader",method = RequestMethod.POST)
    public WebResponseDto buildCascader(){
        WebResponseDto responseDto = new WebResponseDto();
        JSONArray jsonArray = workOrderTypeBo.buildCascader();
        responseDto.setObj(jsonArray);
        return responseDto;
    }
}

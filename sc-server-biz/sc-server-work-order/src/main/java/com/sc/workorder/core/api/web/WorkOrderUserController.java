package com.sc.workorder.core.api.web;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageInfo;
import com.sc.common.dto.PageDto;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.workorder.entity.WoWorkOrderUser;
import com.sc.workorder.entity.WoWorkOrderUserList;
import com.sc.workorder.entity.WoWorkOrderUserSearch;
import com.sc.common.util.cache.DataDictionaryUtil;
import com.sc.workorder.core.bo.WorkOrderUserBo;
import com.sc.workorder.core.service.WoWorkOrderUserService;
import com.sc.common.annotations.WebApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.context.DefaultBusinessContext;
import java.util.Date;
import java.util.Map;

/**
 * @author: wust
 * @date: 2020-04-01 10:18:23
 * @description:
 */
@WebApi
@RequestMapping("/web/WorkOrderUserController")
@RestController
public class WorkOrderUserController {
    @Autowired
    private WoWorkOrderUserService woWorkOrderUserServiceImpl;

    @Autowired
    private WorkOrderUserBo workOrderUserBo;

   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody WoWorkOrderUserSearch search){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       search.setProjectId(ctx.getProjectId());
       search.setCompanyId(ctx.getBranchCompanyId());

       List<WoWorkOrderUserList> list =  woWorkOrderUserServiceImpl.selectByPageNumSize(search,search.getPageDto().getPageNum(),search.getPageDto().getPageSize()) == null ? null
               : (List<WoWorkOrderUserList>)woWorkOrderUserServiceImpl.selectByPageNumSize(search,search.getPageDto().getPageNum(),search.getPageDto().getPageSize());
       if(CollectionUtil.isNotEmpty(list)){
           for (WoWorkOrderUserList woWorkOrderUserList : list) {
               woWorkOrderUserList.setStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),woWorkOrderUserList.getStatus()));
               if(woWorkOrderUserList.getUserId() != null){
                   SysUser user = DataDictionaryUtil.getApplicationPOByPrimaryKey(woWorkOrderUserList.getUserId(),SysUser.class);
                   if(user != null){
                       woWorkOrderUserList.setUserLabel(user.getRealName());
                   }
               }
           }
       }
       PageInfo page = new PageInfo(list);
       PageDto pageDto = new PageDto();
       BeanUtils.copyProperties(page,pageDto);
       responseDto.setPage(pageDto);
       responseDto.setLstDto(list);
       return responseDto;
   }



   @RequestMapping(value = "",method = RequestMethod.POST)
   public WebResponseDto create(@RequestBody Map entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       Long workOrderTypeId = Convert.toLong(entity.get("workOrderTypeId"));
       String description = Convert.toStr(entity.get("description"));
       String userIds = Convert.toStr(entity.get("userIds"));

       String[] userIdArray = userIds.split(",");
       if(userIdArray != null && userIdArray.length > 0){
           List<WoWorkOrderUser>  woWorkOrderUsers = new ArrayList<>();
           for (String s : userIdArray) {
               if(",".equals(s)){
                   continue;
               }

               WoWorkOrderUser woWorkOrderUserSearch = new WoWorkOrderUser();
               woWorkOrderUserSearch.setWorkOrderTypeId(workOrderTypeId);
               woWorkOrderUserSearch.setUserId(Convert.toLong(s));
               woWorkOrderUserSearch.setCompanyId(ctx.getBranchCompanyId());
               woWorkOrderUserSearch.setProjectId(ctx.getProjectId());
               WoWorkOrderUser woWorkOrderUser = woWorkOrderUserServiceImpl.selectOne(woWorkOrderUserSearch);
               if(woWorkOrderUser != null){
                   continue;
               }


               woWorkOrderUser = new WoWorkOrderUser();
               woWorkOrderUser.setWorkOrderTypeId(workOrderTypeId);
               woWorkOrderUser.setUserId(Convert.toLong(s));
               woWorkOrderUser.setStatus("C103301");
               woWorkOrderUser.setDescription(description);
               woWorkOrderUser.setProjectId(ctx.getProjectId());
               woWorkOrderUser.setCompanyId(ctx.getBranchCompanyId());
               woWorkOrderUser.setCreaterId(ctx.getAccountId());
               woWorkOrderUser.setCreaterName(ctx.getAccountName());
               woWorkOrderUser.setCreateTime(new Date());
               woWorkOrderUsers.add(woWorkOrderUser);
           }

           if(CollectionUtil.isNotEmpty(woWorkOrderUsers)){
               woWorkOrderUserServiceImpl.insertList(woWorkOrderUsers);
           }
       }
       return responseDto;
   }


   @RequestMapping(value = "",method = RequestMethod.PUT)
   public WebResponseDto update(@RequestBody  WoWorkOrderUser entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setModifyId(ctx.getAccountId());
       entity.setModifyName(ctx.getAccountName());
       entity.setModifyTime(new Date());
       woWorkOrderUserServiceImpl.updateByPrimaryKeySelective(entity);
       return responseDto;
   }


   @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
   public WebResponseDto delete(@PathVariable Long id){
       WebResponseDto responseDto = new WebResponseDto();
       woWorkOrderUserServiceImpl.deleteByPrimaryKey(id);
       return responseDto;
   }


    @RequestMapping(value = "/buildCascaderByWorkType",method = RequestMethod.POST)
    public WebResponseDto buildCascader(@RequestParam Long workType){
        WebResponseDto responseDto = new WebResponseDto();
        JSONArray jsonArray = workOrderUserBo.buildCascader(workType);
        responseDto.setObj(jsonArray);
        return responseDto;
    }
}

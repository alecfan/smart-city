package com.sc.workorder.core.api.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageInfo;
import com.sc.common.annotations.OperationLog;
import com.sc.common.dto.PageDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.workorder.entity.WoWorkOrder;
import com.sc.workorder.entity.WoWorkOrderList;
import com.sc.workorder.entity.WoWorkOrderSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.CodeGenerator;
import com.sc.common.util.cache.DataDictionaryUtil;
import com.sc.workorder.core.service.WoWorkOrderService;
import com.sc.common.annotations.WebApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.context.DefaultBusinessContext;
import java.util.Date;

/**
 * @author: wust
 * @date: 2020-04-02 08:41:42
 * @description:
 */
@WebApi
@RequestMapping("/web/WorkOrderController")
@RestController
public class WorkOrderController {
    @Autowired
    private WoWorkOrderService woWorkOrderServiceImpl;

   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody WoWorkOrderSearch search){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       search.setProjectId(ctx.getProjectId());
       search.setCompanyId(ctx.getBranchCompanyId());

       List<WoWorkOrderList> list =  woWorkOrderServiceImpl.selectByPageNumSize(search,search.getPageDto().getPageNum(),search.getPageDto().getPageSize()) == null ? null
               : (List<WoWorkOrderList>)woWorkOrderServiceImpl.selectByPageNumSize(search,search.getPageDto().getPageNum(),search.getPageDto().getPageSize());
       if(CollectionUtil.isNotEmpty(list)){
           for (WoWorkOrderList woWorkOrderList : list) {
               SysAccount applicant = DataDictionaryUtil.getApplicationPOByPrimaryKey(woWorkOrderList.getApplicant(), SysAccount.class);
               SysAccount director = DataDictionaryUtil.getApplicationPOByPrimaryKey(woWorkOrderList.getDirector(), SysAccount.class);
               woWorkOrderList.setApplicantLabel(applicant == null ? null : applicant.getAccountName());
               woWorkOrderList.setDirectorLabel(director == null ? null : director.getAccountName());
               woWorkOrderList.setPriorityLevelLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),woWorkOrderList.getPriorityLevel()));
               woWorkOrderList.setStateLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),woWorkOrderList.getState()));
           }
       }
       PageInfo page = new PageInfo(list);
       PageDto pageDto = new PageDto();
       BeanUtils.copyProperties(page,pageDto);
       responseDto.setPage(pageDto);
       responseDto.setLstDto(list);
       return responseDto;
   }



   @OperationLog(moduleName= OperationLogEnum.MODULE_WO_WORK_ORDER,businessName="新建工单",operationType= OperationLogEnum.Insert)
   @RequestMapping(value = "",method = RequestMethod.POST)
   public WebResponseDto create(@RequestBody WoWorkOrder entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setWorkOrderNumber(CodeGenerator.genWorkOrderNumberByCompany());
       entity.setProjectId(ctx.getProjectId());
       entity.setCompanyId(ctx.getBranchCompanyId());
       entity.setCreaterId(ctx.getAccountId());
       entity.setCreaterName(ctx.getAccountName());
       entity.setCreateTime(new Date());
       woWorkOrderServiceImpl.create(entity);
       return responseDto;
   }


    @OperationLog(moduleName= OperationLogEnum.MODULE_WO_WORK_ORDER,businessName="修改工单",operationType= OperationLogEnum.Update)
   @RequestMapping(value = "",method = RequestMethod.PUT)
   public WebResponseDto update(@RequestBody  WoWorkOrder entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setModifyId(ctx.getAccountId());
       entity.setModifyName(ctx.getAccountName());
       entity.setModifyTime(new Date());
       woWorkOrderServiceImpl.updateByPrimaryKeySelective(entity);
       return responseDto;
   }


    @OperationLog(moduleName= OperationLogEnum.MODULE_WO_WORK_ORDER,businessName="删除工单",operationType= OperationLogEnum.Delete)
   @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
   public WebResponseDto delete(@PathVariable Long id){
       WebResponseDto responseDto = new WebResponseDto();
       woWorkOrderServiceImpl.delete(id);
       return responseDto;
   }



    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public WebResponseDto detail(@PathVariable Long id){
        WebResponseDto responseDto = new WebResponseDto();
        WoWorkOrder entity = woWorkOrderServiceImpl.selectByPrimaryKey(id);
        if(entity != null){
            responseDto.setObj(entity);
        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("没有该记录！");
            return responseDto;
        }
        return responseDto;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_WO_WORK_ORDER,businessName="分配工单",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "distribute",method = RequestMethod.POST)
    public WebResponseDto distribute(@RequestParam Long workOrderId, @RequestParam Long director){
        WebResponseDto responseDto = new WebResponseDto();
        woWorkOrderServiceImpl.distribute(workOrderId,director);
        return responseDto;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_WO_WORK_ORDER,businessName="确认工单",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "confirm",method = RequestMethod.POST)
    public WebResponseDto confirm(@RequestParam Long workOrderId, @RequestParam String description){
        WebResponseDto responseDto = new WebResponseDto();
        woWorkOrderServiceImpl.confirm(workOrderId,description);
        return responseDto;
    }
}

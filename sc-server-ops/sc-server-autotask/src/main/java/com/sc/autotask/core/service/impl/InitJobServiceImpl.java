/**
 * Created by wust on 2020-03-31 09:13:01
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.autotask.core.service.impl;


import com.sc.autotask.core.job.BaseJob;
import com.sc.common.annotations.Job;
import com.sc.common.service.InitializtionService;
import com.sc.common.util.SpringContextHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.*;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-31 09:13:01
 * @description:
 *
 */
@Order(3)
@Service
public class InitJobServiceImpl implements InitializtionService {

    static Logger logger = LogManager.getLogger(InitJobServiceImpl.class);

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;


    @Transactional(rollbackFor=Exception.class)
    @Override
    public void init() {
        Map<String, Object> beansWithAnnotationMap = SpringContextHolder.getApplicationContext().getBeansWithAnnotation(Job.class);
        Set<Map.Entry<String, Object>> entrySet =  beansWithAnnotationMap.entrySet();
        for (Map.Entry<String, Object> stringObjectEntry : entrySet) {
            Job jobAnnotation = AopUtils.getTargetClass(stringObjectEntry.getValue()).getAnnotation(Job.class);
            if(jobAnnotation != null){
                String jobName = jobAnnotation.jobName();
                String jobGroupName = jobAnnotation.jobGroupName();
                String jobClassName =  AopUtils.getTargetClass(stringObjectEntry.getValue()).getName();
                String cronExpression = jobAnnotation.cronExpression();
                String jobDescription = jobAnnotation.jobDescription();

                Scheduler scheduler = schedulerFactoryBean.getScheduler();
                try {

                    // 判断job是否已经存在
                    JobKey jobKey = new JobKey(jobName,jobGroupName);
                    boolean isExists = scheduler.checkExists(jobKey);

                    // 判断触发器是否已经存在
                    TriggerKey triggerKey = new TriggerKey(jobName,jobGroupName);
                    boolean isTriggerExists = scheduler.checkExists(triggerKey);

                    // 存在则删除
                    if(isExists || isTriggerExists){
                        scheduler.deleteJob(jobKey);
                    }

                    //构建job信息
                    JobDetail jobDetail = JobBuilder.newJob(getClass(jobClassName).getClass()).withIdentity(jobName, jobGroupName).withDescription(jobDescription).build();

                    //表达式调度构建器(即任务执行的时间)
                    CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);

                    //按新的cronExpression表达式构建一个新的trigger
                    CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName, jobGroupName).withSchedule(scheduleBuilder).build();

                    scheduler.scheduleJob(jobDetail, trigger);

                    if(!scheduler.isShutdown()){
                        scheduler.start();
                    }
                } catch (SchedulerException e) {
                    logger.error("创建作业失败：" + e);
                } catch (Exception e) {
                    logger.error("创建作业失败：" + e);
                }
            }
        }
    }




    private BaseJob getClass(String classname) throws Exception {
        Class<?> class1 = Class.forName(classname);
        return (BaseJob)class1.newInstance();
    }
}

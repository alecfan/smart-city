package com.sc.autotask.core.dao;

import com.sc.autotask.entity.jobandtrigger.QrtzJobAndTriggerList;
import com.sc.autotask.entity.jobandtrigger.QrtzJobAndTriggerSearch;
import org.apache.ibatis.annotations.Param;
import org.springframework.dao.DataAccessException;
import java.util.List;

/**
 * Created by wust on 2019/6/13.
 */
public interface JobMapper {
    List<QrtzJobAndTriggerList> selectByPageNumSize(
                                            @Param("search")QrtzJobAndTriggerSearch search,
                                            @Param("pageNum") int pageNum,
                                            @Param("pageSize") int pageSize
    ) throws DataAccessException;
}

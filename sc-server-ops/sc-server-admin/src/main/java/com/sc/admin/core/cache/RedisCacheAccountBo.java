/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysAccountMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 账号缓存，如果账号有变动，则MQ会收到消息，然后MQ调用此类相关方法进行缓存同步
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysAccount")
@Component
public class RedisCacheAccountBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysAccountMapper sysAccountMapper;

    /**
     * 系统启动时会调用此方法初始化缓存
     */
    @Override
    public void init() {
        Set<String> keys = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ACCOUNT_BY_CODE.getStringValue().replaceAll("%s_","*"));
        if(keys != null && keys.size() > 0){
            springRedisTools.deleteByKey(keys);
        }

        List<SysAccount> accountList =  sysAccountMapper.selectAll();
        if(CollectionUtil.isNotEmpty(accountList)){
            for (SysAccount account : accountList) {
               cacheByCode(account);
            }
        }
    }

    /**
     * 如果需要重置缓存，可在此方法编写重置缓存的代码
     */
    @Override
    public void reset() {

    }

    /**
     * 新增缓存
     * @param obj
     */
    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        SysAccount entity = null;

        if(obj instanceof Long){
            entity = sysAccountMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof SysAccount){
            entity = (SysAccount)obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),SysAccount.class);
        }

        if(entity == null){
            return;
        }

        cacheByCode(entity);
    }

    /**
     * 批量新增缓存
     * @param list
     */
    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    /**
     * 更新缓存
     * @param primaryKey
     */
    @Override
    public void updateByPrimaryKey(Object primaryKey){
        SysAccount account = sysAccountMapper.selectByPrimaryKey(primaryKey);
        if(account != null){
           cacheByCode(account);
        }
    }

    /**
     * 批量更新缓存
     * @param list
     */
    @Override
    public void batchUpdate(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    /**
     * 删除缓存
     * @param primaryKey
     */
    @Override
    public void deleteByPrimaryKey(Object primaryKey){
        SysAccount account = sysAccountMapper.selectByPrimaryKey(primaryKey);
        if(account != null){
            String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ACCOUNT_BY_CODE.getStringValue(),account.getAccountCode());
            if(springRedisTools.hasKey(key1)){
                springRedisTools.deleteByKey(key1);
            }
        }
    }


    /**
     * 批量删除缓存
     * @param primaryKeys
     */
    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }


    private void cacheByCode(SysAccount account){
        if(account == null){
            return;
        }
        String code = account.getAccountCode();
        String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_ACCOUNT_BY_CODE.getStringValue(),code);
        if(springRedisTools.hasKey(key1)){
            springRedisTools.deleteByKey(key1);
        }
        Map mapValue1 = BeanUtil.beanToMap(account);
        springRedisTools.addMap(key1,mapValue1);
    }
}

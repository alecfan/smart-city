package com.sc.admin.core.api.web;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.admin.core.service.SysLookupService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.entity.admin.lookup.SysLookupList;
import com.sc.common.entity.admin.lookup.SysLookupSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：wust
 * @date ：Created in 2019/7/26 14:48
 * @description：
 * @version:
 */
@WebApi
@RequestMapping("/web/LookupController")
@RestController
public class LookupController {
    @Autowired
    private SysLookupService sysLookupServiceImpl;


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_LOOKUP,businessName="构建数据字典树",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/buildLookupTree",method = RequestMethod.POST)
    public WebResponseDto buildLookupTree(){
        WebResponseDto baseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        JSONArray jsonArray = new JSONArray();
        JSONObject rootJSONObject = new JSONObject();
        jsonArray.add(rootJSONObject);
        rootJSONObject.put("id","0000");
        rootJSONObject.put("pId",null);
        rootJSONObject.put("rootCode",null);
        rootJSONObject.put("lookupId","-1");
        rootJSONObject.put("name","数据字典");
        rootJSONObject.put("open",true);

        SysLookupSearch sysLookupSearch = new SysLookupSearch();
        sysLookupSearch.setLan(ctx.getLocale().toString());
        List<SysLookup> sysLookupLists =  sysLookupServiceImpl.select(sysLookupSearch);
        if(CollectionUtils.isNotEmpty(sysLookupLists)){
            for (SysLookup sysLookup : sysLookupLists) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id",sysLookup.getCode());
                jsonObject.put("pId",sysLookup.getParentCode());
                jsonObject.put("rootCode",sysLookup.getRootCode());
                jsonObject.put("lookupId",sysLookup.getId());
                jsonObject.put("name",sysLookup.getDescription());
                jsonObject.put("open",false);
                jsonArray.add(jsonObject);
            }
            baseDto.setObj(jsonArray.toJSONString());
        }
        return baseDto;
    }

    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysLookupSearch search){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        search.setLan(ctx.getLocale().toString());

        PageDto pageDto = search.getPageDto();
        Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());
        List<SysLookup>  lookups = sysLookupServiceImpl.select(search);
        if (CollectionUtils.isNotEmpty(lookups)) {
            List<SysLookupList> lookupLists = new ArrayList<>(lookups.size());
            for (SysLookup lookup : lookups) {
                SysLookupList lookupList = new SysLookupList();
                BeanUtils.copyProperties(lookup,lookupList);
                lookupList.setVisibleLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),lookup.getVisible()));
                lookupList.setStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),lookup.getStatus()));
                lookupLists.add(lookupList);
            }
            responseDto.setLstDto(lookupLists);

        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("没有找到数据字典记录");
        }
        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        return responseDto;
    }
}

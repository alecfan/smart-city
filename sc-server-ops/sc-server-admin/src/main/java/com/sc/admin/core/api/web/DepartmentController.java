package com.sc.admin.core.api.web;


import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageInfo;
import com.sc.admin.core.service.SysDepartmentService;
import com.sc.admin.core.service.SysOrganizationService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.department.SysDepartmentList;
import com.sc.common.entity.admin.department.SysDepartmentSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created by wust on 2019/6/3.
 */
@WebApi
@RequestMapping("/web/DepartmentController")
@RestController
public class DepartmentController {
    @Autowired
    private SysDepartmentService sysDepartmentServiceImpl;

    @Autowired
    private SysOrganizationService sysOrganizationServiceImpl;



    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_DEPARTMENT,businessName="分页查询",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysDepartmentSearch search){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        if(ctx.isStaff()){ // 非管理员只能查看指定范围的数据
            search.setAgentId(ctx.getAgentId());
            search.setParentCompanyId(ctx.getParentCompanyId());
            search.setBranchCompanyId(ctx.getBranchCompanyId());
            search.setProjectId(ctx.getProjectId());
        }

        PageDto pageDto = search.getPageDto();
        List<SysDepartmentList> list =  sysDepartmentServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize()) == null ? null
                : (List<SysDepartmentList>)sysDepartmentServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize());
        if(CollectionUtil.isNotEmpty(list)){
            for (SysDepartmentList departmentList : list) {
                departmentList.setTypeLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),departmentList.getType()));
            }
        }
        PageInfo page = new PageInfo(list);
        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        responseDto.setLstDto(list);
        return responseDto;
    }
}

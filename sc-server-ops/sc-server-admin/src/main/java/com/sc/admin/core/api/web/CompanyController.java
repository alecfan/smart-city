package com.sc.admin.core.api.web;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.admin.core.service.SysCompanyService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.company.SysCompany;
import com.sc.common.entity.admin.company.SysCompanyList;
import com.sc.common.entity.admin.company.SysCompanySearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wust on 2019/6/3.
 */
@WebApi
@RequestMapping("/web/CompanyController")
@RestController
public class CompanyController {

    @Autowired
    private SysCompanyService sysCompanyServiceImpl;


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_COMPANY,businessName="分页查询",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysCompanySearch search){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        WebResponseDto responseDto = new WebResponseDto();

        PageDto pageDto = search.getPageDto();
        Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());

        List<SysCompany> companies =  sysCompanyServiceImpl.select(search);
        if(CollectionUtils.isNotEmpty(companies)){
            List<SysCompanyList> companyLists = new ArrayList<>(companies.size());
            for (SysCompany company : companies) {
                SysCompanyList companyList = new SysCompanyList();
                BeanUtils.copyProperties(company,companyList);
                companyList.setTypeLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),company.getType()));
                companyLists.add(companyList);
            }
            responseDto.setLstDto(companyLists);
        }
        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        return responseDto;
    }
}

package com.sc.admin.core.api.web;

import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.lookup.SysLookup;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.cache.SpringRedisTools;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created by wust on 2019/5/28.
 */
@WebApi
@RequestMapping("/web/DataDictionaryController")
@RestController
public class DataDictionaryController {
    @Autowired
    private SpringRedisTools springRedisTools;


    @RequestMapping(value = "/getLookupListByParentCode/{parentCode}/{defaultValue}",method = RequestMethod.POST)
    public WebResponseDto getLookupListByParentCode(@PathVariable String parentCode, @PathVariable String defaultValue){
        WebResponseDto baseDto = new WebResponseDto();
        WebResponseDto mm = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        List<SysLookup> lookupList = DataDictionaryUtil.getLookupListByParentCode(ctx.getLocale().toString(),parentCode);
        if (CollectionUtils.isNotEmpty(lookupList)) {
            StringBuffer html = new StringBuffer();
            boolean hasDefaultValueFlag = false;
            for (SysLookup lookup : lookupList) {
                String visible = lookup.getVisible();
                String value = lookup.getCode();
                String label = lookup.getName();

                if("A100702".equals(visible)){
                    // 不显示
                    continue;
                }

                if (MyStringUtils.isNotBlank(MyStringUtils.null2String(defaultValue)) && MyStringUtils.null2String(defaultValue).equals(value)) {
                    hasDefaultValueFlag = true;
                    html.append("<option value=" + value + " selected>" + label + "</option>");
                } else {
                    html.append("<option value=" + value + ">" + label + "</option>");
                }
            }
            String result = "";
            if (!hasDefaultValueFlag) {
                result = "<option value=null selected>--请选择--</option>" + html.toString();
            } else {
                result = "<option value=null>--请选择--</option>" + html.toString();
            }
            baseDto.setT(result);
        }else{
            mm.setFlag(WebResponseDto.INFO_WARNING);
            mm.setMessage("没有找到数据字典记录");
        }
        return baseDto;
    }
}

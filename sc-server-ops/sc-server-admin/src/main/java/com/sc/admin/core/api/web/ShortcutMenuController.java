package com.sc.admin.core.api.web;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.sc.admin.core.service.SysMenuService;
import com.sc.admin.core.service.SysShortcutMenuService;
import com.sc.common.annotations.WebApi;
import com.sc.common.dto.PageDto;
import com.sc.common.entity.admin.menu.SysMenu;
import com.sc.common.entity.admin.shortcutmenu.SysShortcutMenu;
import com.sc.common.entity.admin.shortcutmenu.SysShortcutMenuList;
import com.sc.common.entity.admin.shortcutmenu.SysShortcutMenuSearch;
import com.sc.common.util.MyStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.context.DefaultBusinessContext;
import java.util.Date;

/**
 * @author: wust
 * @date: 2020-02-16 11:25:19
 * @description:
 */
@WebApi
@RequestMapping("/web/ShortcutMenuController")
@RestController
public class ShortcutMenuController {
    @Autowired
    private SysShortcutMenuService sysShortcutMenuServiceImpl;

    @Autowired
    private SysMenuService sysMenuServiceImpl;

   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody SysShortcutMenuSearch search){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       search.setUserId(ctx.getAccountId());

       PageDto pageDto = search.getPageDto();
       List<SysShortcutMenuList> list =  sysShortcutMenuServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize()) == null ? null
               : (List<SysShortcutMenuList>)sysShortcutMenuServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize());
       PageInfo page = new PageInfo(list);
       BeanUtils.copyProperties(page,pageDto);
       responseDto.setPage(pageDto);
       responseDto.setLstDto(list);
       return responseDto;
   }


    @RequestMapping(value = "/buildMenuTree",method = RequestMethod.POST)
    public WebResponseDto buildTree(){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        JSONArray jsonArray = new JSONArray();

        List<SysMenu> menus = null;
        if(ctx.isSuperAdmin()) { // 平台超级管理员
            menus = sysMenuServiceImpl.findAsideMenu4superAdmin(ctx.getPermissionType(),ctx.getLocale().toString());
        }else if(ctx.isAdmin()){ // 平台普通管理员
            menus = sysMenuServiceImpl.findAsideMenu4admin(ctx.getPermissionType(),ctx.getLocale().toString(),ctx.getAccountId());
        }else if(ctx.isStaff()){ // 员工
            menus = sysMenuServiceImpl.findAsideMenuByUserId(ctx.getPermissionType(),ctx.getLocale().toString(),ctx.getAccountId());
        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("非法的用户");
            return responseDto;
        }

        if(CollectionUtil.isNotEmpty(menus)){
            for (SysMenu menu : menus) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id",menu.getCode());
                jsonObject.put("pId",menu.getPcode());
                jsonObject.put("name",menu.getDescription());
                jsonObject.put("menuId",menu.getId());
                if(MyStringUtils.isBlank(MyStringUtils.null2String(menu.getPcode()))){
                    jsonObject.put("open",true);
                }
                if("A100701".equals(menu.getIsParent())){ // 是父节点
                    jsonObject.put("nocheck", true);
                }
                jsonArray.add(jsonObject);
            }
        }

        responseDto.setObj(jsonArray.toJSONString());
        return responseDto;
    }

   @RequestMapping(value = "",method = RequestMethod.POST)
   public WebResponseDto create(@RequestBody  SysShortcutMenu entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setUserId(ctx.getAccountId());
       entity.setCreaterId(ctx.getAccountId());
       entity.setCreaterName(ctx.getAccountName());
       entity.setCreateTime(new Date());
       sysShortcutMenuServiceImpl.insert(entity);
       return responseDto;
   }


   @RequestMapping(value = "",method = RequestMethod.PUT)
   public WebResponseDto update(@RequestBody  SysShortcutMenu entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setModifyId(ctx.getAccountId());
       entity.setModifyName(ctx.getAccountName());
       entity.setModifyTime(new Date());
       sysShortcutMenuServiceImpl.updateByPrimaryKeySelective(entity);
       return responseDto;
   }


   @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
   public WebResponseDto delete(@PathVariable Long id){
       WebResponseDto responseDto = new WebResponseDto();
       sysShortcutMenuServiceImpl.deleteByPrimaryKey(id);
       return responseDto;
   }
}

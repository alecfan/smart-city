package com.sc.admin.core.api.web;

import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：wust
 * @date ：Created in 2019/7/24 14:01
 * @description：
 * @version:
 */
@Api(tags = {"Web接口~员工登出"})
@WebApi
@RequestMapping("/web/UserLogoutController")
@RestController
public class LogoutController {

    @Autowired
    private SpringRedisTools springRedisTools;


    @ApiOperation(value = "Web用户登出", httpMethod = "POST")
    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="Web用户登出",operationType= OperationLogEnum.Logout)
    @RequestMapping(value = "/logout/{xAuthToken}",method = RequestMethod.POST)
    public WebResponseDto logout(@PathVariable String xAuthToken) {
        WebResponseDto responseDto = new WebResponseDto();

        String key = xAuthToken;
        if(springRedisTools.hasKey(key)){
            springRedisTools.deleteByKey(key);
        }

        return responseDto;
    }
}

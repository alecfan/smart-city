package com.sc.admin.core.service;

import com.sc.common.entity.admin.apptoken.SysAppToken;
import com.sc.common.service.BaseService;

public interface SysAppTokenService extends BaseService<SysAppToken> {
}

package com.sc.admin.core.api.web;


import com.sc.admin.core.service.SysUserOrganizationService;
import com.sc.common.annotations.WebApi;
import com.sc.common.dto.WebResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wust on 2019/6/5.
 */
@WebApi
@RequestMapping("/web/SysUserOrganization")
@RestController
public class SysUserOrganizationController {

    @Autowired
    private SysUserOrganizationService sysUserOrganizationServiceImpl;

    @Autowired
    private Environment env;



    /**
     * 初始化用户组织关系
     * @return
     */
    @RequestMapping(value = "/init",method = RequestMethod.POST)
    public WebResponseDto init() {
        WebResponseDto responseDto = new WebResponseDto();
        sysUserOrganizationServiceImpl.init();
        return responseDto;
    }
}

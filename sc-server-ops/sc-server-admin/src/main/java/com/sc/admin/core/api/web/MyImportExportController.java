package com.sc.admin.core.api.web;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.admin.core.service.SysImportExportService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.importexport.SysImportExport;
import com.sc.common.entity.admin.importexport.SysImportExportList;
import com.sc.common.entity.admin.importexport.SysImportExportSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.service.MinioStorageApiService;
import com.sc.common.service.MinioStorageService;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wust on 2019/5/17.
 */
@WebApi
@RequestMapping("/web/MyImportExportController")
@RestController
public class MyImportExportController {
    @Autowired
    private SysImportExportService sysImportExportServiceImpl;

    @Autowired
    private Environment environment;

    @Autowired
    private MinioStorageApiService minioStorageApiService;

    @Autowired
    private MinioStorageService minioStorageServiceImpl;

    @OperationLog(moduleName = OperationLogEnum.MODULE_ADMIN_IMPORT_EXPORT, businessName = "分页查询导入导出列表", operationType = OperationLogEnum.Search)
    @RequestMapping(value = "/listPage", method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysImportExportSearch search) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        search.setCreaterId(ctx.getAccountId());

        PageDto pageDto = search.getPageDto();
        Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());

        List<SysImportExport> importExports = sysImportExportServiceImpl.select(search);
        if (CollectionUtils.isNotEmpty(importExports)) {
            List<SysImportExportList> importExportLists = new ArrayList<>(importExports.size());
            for (SysImportExport importExport : importExports) {
                SysImportExportList importExportList = new SysImportExportList();
                BeanUtils.copyProperties(importExport,importExportList);
                importExportList.setStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(), importExport.getStatus()));
                importExportList.setOperationTypeLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(), importExport.getOperationType()));
                importExportLists.add(importExportList);
            }
            responseDto.setLstDto(importExportLists);
        }

        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        return responseDto;
    }

    @OperationLog(moduleName = OperationLogEnum.MODULE_ADMIN_IMPORT_EXPORT, businessName = "下载", operationType = OperationLogEnum.Download)
    @RequestMapping(value = "/downloadFile")
    public WebResponseDto downloadFile(HttpServletRequest request, HttpServletResponse response) {
        WebResponseDto responseDto = new WebResponseDto();
        String batchNo = request.getParameter("batchNo");
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();

            SysImportExportSearch sysImportExportSearch = new SysImportExportSearch();
            sysImportExportSearch.setBatchNo(batchNo);
            SysImportExport sysImportExport = sysImportExportServiceImpl.selectOne(sysImportExportSearch);
            if(sysImportExport != null){
                response.setHeader("Content-Disposition","attachment;filename=" + sysImportExport.getName());
                InputStream inputStream = minioStorageApiService.download(sysImportExport.getFileId(),minioStorageServiceImpl);
                IOUtils.copy(inputStream,outputStream);
            }else{
                response.setHeader("Content-Disposition","attachment;filename=error.xls");
            }
        } catch (Exception e) {
            responseDto.setFlag(WebResponseDto.INFO_ERROR);
            responseDto.setMessage("系统异常!");
        }finally {
            if(outputStream != null){
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return responseDto;
    }
}

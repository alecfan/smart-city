package com.sc.admin.core.api.web;


import com.github.pagehelper.PageInfo;
import com.sc.admin.core.service.SysOrganizationService;
import com.sc.admin.core.service.SysProjectService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.project.SysProjectList;
import com.sc.common.entity.admin.project.SysProjectSearch;
import com.sc.common.enums.OperationLogEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author ：wust
 * @date ：Created in 2019/7/30 14:27
 * @description：
 * @version:
 */
@WebApi
@RequestMapping("/web/ProjectController")
@RestController
public class ProjectController {
    @Autowired
    private SysProjectService sysProjectServiceImpl;

    @Autowired
    private SysOrganizationService sysOrganizationServiceImpl;


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_PROJECT,businessName="分页查询",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysProjectSearch search){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        if(ctx.isStaff()){ // 非管理员只能查看指定范围的数据
            search.setAgentId(ctx.getAgentId());
            search.setParentCompanyId(ctx.getParentCompanyId());
            search.setBranchCompanyId(ctx.getBranchCompanyId());
        }

        PageDto pageDto = search.getPageDto();
        List<SysProjectList> list =  sysProjectServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize()) == null ? null
                : (List<SysProjectList>)sysProjectServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize());
        PageInfo page = new PageInfo(list);
        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        responseDto.setLstDto(list);
        return responseDto;
    }
}

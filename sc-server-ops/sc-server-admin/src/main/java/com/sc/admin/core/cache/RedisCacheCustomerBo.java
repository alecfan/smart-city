/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysCustomerMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.customer.SysCustomer;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 客户缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysCustomer")
@Component
public class RedisCacheCustomerBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysCustomerMapper sysCustomerMapper;

    @Override
    public void init() {
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        SysCustomer entity = null;

        if(obj instanceof Long){
            entity = sysCustomerMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof SysCustomer){
            entity = (SysCustomer)obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),SysCustomer.class);
        }

        if(entity == null){
            return;
        }

    }

    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){

    }

    @Override
    public void batchUpdate(List<Object> list){

    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey){
    }

    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }
}

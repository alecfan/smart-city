package com.sc.admin.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysAccountMapper;
import com.sc.admin.core.dao.SysCustomerMapper;
import com.sc.admin.core.service.SysCustomerService;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.customer.SysCustomer;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author: wust
 * @date: 2019-12-27 11:37:51
 * @description:
 */
@Service("sysCustomerServiceImpl")
public class SysCustomerServiceImpl extends BaseServiceImpl<SysCustomer> implements SysCustomerService {
    @Autowired
    private SysCustomerMapper sysCustomerMapper;

    @Autowired
    private SysAccountMapper sysAccountMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysCustomerMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        JSONObject jsonObject = (JSONObject)obj;

        SysAccount account = jsonObject.getObject("account",SysAccount.class);
        SysCustomer customer = jsonObject.getObject("customer",SysCustomer.class);

        this.sysCustomerMapper.insert(customer);

        this.sysAccountMapper.insert(account);
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();

        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        SysCustomer customer = (SysCustomer)obj;

        SysAccount account =  sysAccountMapper.selectByPrimaryKey(customer.getId());
        account.setAccountName(customer.getName());
        account.setContactNumber(customer.getMobilePhone());
        account.setEmail(customer.getEmail());
        account.setModifyId(ctx.getAccountId());
        account.setModifyName(ctx.getAccountName());
        account.setModifyTime(new Date());
        sysAccountMapper.updateByPrimaryKeySelective(account);

        sysCustomerMapper.updateByPrimaryKeySelective(customer);
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        SysAccount account = sysAccountMapper.selectByPrimaryKey(obj);
        if(account != null){
            account.setIsDeleted(1);
            account.setModifyId(ctx.getAccountId());
            account.setModifyName(ctx.getAccountName());
            account.setModifyTime(new Date());
            sysAccountMapper.updateByPrimaryKeySelective(account);
        }

        SysCustomer customer = sysCustomerMapper.selectByPrimaryKey(obj);
        if(customer != null){
            customer.setIsDeleted(1);
            customer.setModifyId(ctx.getAccountId());
            customer.setModifyName(ctx.getAccountName());
            customer.setModifyTime(new Date());
            sysCustomerMapper.updateByPrimaryKeySelective(customer);
        }
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void registerAccount(SysAccount account, SysCustomer customer) {
        this.sysCustomerMapper.insert(customer);
        account.setId(customer.getId());
        this.sysAccountMapper.insert(account);
    }
}

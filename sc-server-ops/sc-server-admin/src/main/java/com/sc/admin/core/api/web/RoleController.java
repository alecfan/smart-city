package com.sc.admin.core.api.web;


import com.github.pagehelper.PageInfo;
import com.sc.admin.core.service.SysRoleService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.role.SysRoleList;
import com.sc.common.entity.admin.role.SysRoleSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created by wust on 2019/5/27.
 */
@WebApi
@RequestMapping("/web/RoleController")
@RestController
public class RoleController {
    @Autowired
    private SysRoleService sysRoleServiceImpl;



    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_ROLE,businessName="分页查询",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysRoleSearch search){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        if(ctx.isStaff()){ // 非管理员只能查看指定范围的数据
            search.setAgentId(ctx.getAgentId());
            search.setParentCompanyId(ctx.getParentCompanyId());
            search.setBranchCompanyId(ctx.getBranchCompanyId());
            search.setProjectId(ctx.getProjectId());
        }

        PageDto pageDto = search.getPageDto();
        List<SysRoleList> list =  sysRoleServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize()) == null ? null
                : (List<SysRoleList>)sysRoleServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize());
        if(CollectionUtils.isNotEmpty(list)){
            for (SysRoleList roleList : list) {
                roleList.setStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),roleList.getStatus()));
            }
        }
        PageInfo page = new PageInfo(list);
        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        responseDto.setLstDto(list);
        return responseDto;
    }
}

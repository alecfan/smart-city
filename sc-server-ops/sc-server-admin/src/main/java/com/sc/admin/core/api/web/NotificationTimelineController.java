package com.sc.admin.core.api.web;

import com.sc.admin.core.service.SysNotificationTimelineService;
import com.sc.common.entity.admin.notification.SysNotificationTimeline;
import com.sc.common.annotations.WebApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-04-09 13:26:08
 * @description:
 */
@WebApi
@RequestMapping("/web/NotificationTimelineController")
@RestController
public class NotificationTimelineController {
    @Autowired
    private SysNotificationTimelineService sysNotificationTimelineServiceImpl;




    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public WebResponseDto detail(@PathVariable Long id){
        WebResponseDto responseDto = new WebResponseDto();
        SysNotificationTimeline entity = sysNotificationTimelineServiceImpl.selectByPrimaryKey(id);
        if(entity != null){
            responseDto.setObj(entity);
        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("没有该记录！");
            return responseDto;
        }
        return responseDto;
    }
}

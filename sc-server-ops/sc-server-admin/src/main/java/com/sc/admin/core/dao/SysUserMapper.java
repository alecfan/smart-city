package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.entity.admin.user.SysUserList;
import com.sc.common.entity.admin.userorganization.SysUserOrganizationSearch;
import org.apache.ibatis.annotations.Param;
import org.springframework.dao.DataAccessException;
import java.util.List;

/**
 * Created by wust on 2019/4/18.
 */
public interface SysUserMapper   extends IBaseMapper<SysUser> {
    List<SysUserList> listPageByParentOrganization(
            @Param("search") SysUserOrganizationSearch search,
            @Param("pageNum") int pageNum,
            @Param("pageSize") int pageSize
    ) throws DataAccessException;
}

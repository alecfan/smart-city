/**
 * Created by wust on 2020-03-16 14:17:39
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.api.web;

import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.service.SysCompanyService;
import com.sc.admin.core.service.SysProjectService;
import com.sc.admin.core.service.SysUserService;
import com.sc.common.annotations.WebApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.company.SysCompany;
import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-16 14:17:39
 * @description: 仪表盘专用
 *
 */
@WebApi
@RequestMapping("/web/DashboardController")
@RestController
public class DashboardController {
    @Autowired
    private SysCompanyService sysCompanyServiceImpl;

    @Autowired
    private SysProjectService sysProjectServiceImpl;

    @Autowired
    private SysUserService sysUserServiceImpl;

    @Autowired
    private SpringRedisTools springRedisTools;


    @RequestMapping(value = "/detailForA100401",method = RequestMethod.GET)
    public WebResponseDto detailForA100401() {
        WebResponseDto responseDto = new WebResponseDto();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("agentCount",0);
        jsonObject.put("parentCompanyCount",0);
        jsonObject.put("branchCompanyCount",0);
        jsonObject.put("projectCount",0);

        jsonObject.put("userCount",0);
        jsonObject.put("activeUserCount",0);

        jsonObject.put("customerCount",0);
        jsonObject.put("activeCustomerCount",0);

        jsonObject.put("employeeCount",0);
        jsonObject.put("enableEmployeeCount",0);
        jsonObject.put("disableEmployeeCount",0);

        jsonObject.put("enableCustomerCount",0);

        SysCompany companySearch = new SysCompany();
        companySearch.setType("A101101");
        int agentCount = sysCompanyServiceImpl.selectCount(companySearch);
        jsonObject.put("agentCount",agentCount);

        companySearch.setType("A101104");
        int parentCompanyCount = sysCompanyServiceImpl.selectCount(companySearch);
        jsonObject.put("parentCompanyCount",parentCompanyCount);

        companySearch.setType("A101107");
        int branchCompanyCount = sysCompanyServiceImpl.selectCount(companySearch);
        jsonObject.put("branchCompanyCount",branchCompanyCount);

        SysProject projectSearch = new SysProject();
        int projectCount = sysProjectServiceImpl.selectCount(projectSearch);
        jsonObject.put("projectCount",projectCount);


        SysUser userSearch = new SysUser();
        int userCount = sysUserServiceImpl.selectCount(userSearch);
        Set set = springRedisTools.keys("WEB_LOGIN_KEY*");
        int activeUserCount = set == null ? 0 : set.size();
        jsonObject.put("userCount",userCount);
        jsonObject.put("activeUserCount",activeUserCount);

        // 员工总数，减1是因为有一个是系统管理员
        jsonObject.put("employeeCount",userCount > 0 ? userCount - 1 : 0);

        // 启用的员工，减1是因为有一个是系统管理员
        userSearch.setStatus("A102901"); // 启用
        int enableEmployeeCount = sysUserServiceImpl.selectCount(userSearch);
        jsonObject.put("enableEmployeeCount",enableEmployeeCount > 0 ? enableEmployeeCount - 1 : 0);

        // 禁用的员工，减1是因为有一个是系统管理员
        userSearch.setStatus("A102903");
        int disableEmployeeCount = sysUserServiceImpl.selectCount(userSearch);
        jsonObject.put("disableEmployeeCount",disableEmployeeCount > 0 ? disableEmployeeCount - 1 : 0);

        responseDto.setObj(jsonObject);
        return responseDto;
    }
}

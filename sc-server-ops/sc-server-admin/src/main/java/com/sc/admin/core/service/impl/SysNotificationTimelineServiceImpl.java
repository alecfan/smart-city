package com.sc.admin.core.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.bo.NotificationBo;
import com.sc.admin.core.dao.SysMyNoticeMapper;
import com.sc.admin.core.dao.SysNotificationMapper;
import com.sc.admin.core.dao.SysNotificationTimelineMapper;
import com.sc.admin.core.service.SysNotificationTimelineService;
import com.sc.common.entity.admin.notification.SysNotificationTimeline;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.mynotice.SysMyNotice;
import com.sc.common.entity.admin.notification.SysNotification;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;
import tk.mybatis.mapper.entity.Example;
import java.util.Date;
import java.util.List;

/**
 * @author: wust
 * @date: 2020-04-09 13:26:08
 * @description:
 */
@Service("sysNotificationTimelineServiceImpl")
public class SysNotificationTimelineServiceImpl extends BaseServiceImpl<SysNotificationTimeline> implements SysNotificationTimelineService {
    @Autowired
    private SysNotificationTimelineMapper sysNotificationTimelineMapper;

    @Autowired
    private SysNotificationMapper sysNotificationMapper;

    @Autowired
    private SysMyNoticeMapper sysMyNoticeMapper;

    @Autowired
    private NotificationBo notificationBo;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysNotificationTimelineMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto createByMq(JSONObject jsonObject) {
        WebResponseDto responseDto = new WebResponseDto();

        SysNotificationTimeline entity = jsonObject.getObject("entity",SysNotificationTimeline.class);

        String title = jsonObject.getString("title");

        String content = jsonObject.getString("content");

        Long projectId = jsonObject.getLong("projectId");

        Long notificationId = null;

        boolean isNew = false; // 是否是全新消息

        SysNotificationTimeline notificationTimelineSearch = new SysNotificationTimeline();
        notificationTimelineSearch.setRelationCode(entity.getRelationCode());
        List<SysNotificationTimeline> notificationTimelines = sysNotificationTimelineMapper.select(notificationTimelineSearch);
        if(CollectionUtil.isNotEmpty(notificationTimelines)){
            notificationId = notificationTimelines.get(0).getNotificationId();
        }

        if(notificationId == null){ // 该relationCode第一次报警
            SysNotification notification = new SysNotification();
            notification.setTitle(title);
            notification.setStatus("A101301"); // 新建
            notification.setContent(content);
            notification.setPriorityLevel("A101601"); // 优先级：高
            notification.setReceiverType("A101403"); // 接收对象为指定项目下面的所有用户
            notification.setReceiver(Convert.toStr(projectId)); // 项目id
            notification.setSendChannel("A101901,A101902,A101903,A101904,A101905"); // 消息推送渠道
            notification.setType("A102108"); // 消息类型：报警
            notification.setCreaterName("mqtt自动执行");
            notification.setCreateTime(new Date());
            sysNotificationMapper.insert(notification);
            notificationId = notification.getId();
            isNew = true;
        }else{ // 非第一次报警，需要合并
            isNew = false;
        }


        /**
         * 记录报警信息
         */
        entity.setNotificationId(notificationId);
        sysNotificationTimelineMapper.insert(entity);


        if(isNew){ // 新消息，设备第一次报警：发布消息&发出报警
            notificationBo.publish(notificationId);
        }else{ // 非全新消息，即数据库已经存在该消息，即一个设备的多次报警：修改我的通知记录为未读，并且发出报警
            // 修改报警消息为未读
            Example example = new Example(SysMyNotice.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("notificationId", notificationId);
            SysMyNotice myNotice = new SysMyNotice();
            myNotice.setStatus("A101502"); // 修改为未读
            myNotice.setModifyTime(new Date());
            sysMyNoticeMapper.updateByExampleSelective(myNotice,example);


            // 只发出报警
            SysNotification notification = sysNotificationMapper.selectByPrimaryKey(notificationId);
            notificationBo.pushToMyNoticeQueue(notification,"1");
        }
        return responseDto;
    }
}

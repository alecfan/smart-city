package com.sc.admin.core.api.web;


import com.github.pagehelper.PageInfo;
import com.sc.admin.core.service.SysAppTokenService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.apptoken.SysAppToken;
import com.sc.common.entity.admin.apptoken.SysAppTokenList;
import com.sc.common.entity.admin.apptoken.SysAppTokenSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@WebApi
@RequestMapping("/web/AppTokenController")
@RestController
public class AppTokenController {
    @Autowired
    private SysAppTokenService sysAppTokenServiceImpl;


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_APP_TOKEN,businessName="分页查询",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysAppTokenSearch search){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        WebResponseDto responseDto = new WebResponseDto();

        PageDto pageDto = search.getPageDto();
        List<SysAppTokenList> list =  sysAppTokenServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize()) == null ? null
                : (List<SysAppTokenList>)sysAppTokenServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize());
        if(CollectionUtils.isNotEmpty(list)){
            for (SysAppTokenList appTokenList : list) {
                appTokenList.setAppId(appTokenList.getAppId().substring(0,appTokenList.getAppId().length() - 15) + "***");
                appTokenList.setSignSecretKey("***");
                appTokenList.setSignDuration(appTokenList.getSignDuration() / 1000 / 60); // 转为分钟
                appTokenList.setStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),appTokenList.getStatus()));
            }
        }
        PageInfo page = new PageInfo(list);
        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        responseDto.setLstDto(list);
        return responseDto;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_APP_TOKEN,businessName="新建",operationType= OperationLogEnum.Insert)
    @RequestMapping(value = "",method = RequestMethod.POST)
    public WebResponseDto create(@RequestBody SysAppToken entity){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        entity.setAppId(UUID.randomUUID().toString().replaceAll("-","").toUpperCase());
        entity.setSignSecretKey(UUID.randomUUID().toString().replaceAll("-","").toUpperCase());
        entity.setStatus("A101202");
        entity.setSignDuration(entity.getSignDuration() * 60 * 1000);
        entity.setCreaterId(ctx.getAccountId());
        entity.setCreaterName(ctx.getAccountName());
        entity.setCreateTime(new Date());
        sysAppTokenServiceImpl.create(entity);
        return responseDto;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_APP_TOKEN,businessName="修改",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "",method = RequestMethod.PUT)
    public WebResponseDto update(@RequestBody SysAppToken entity){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        entity.setSignDuration(entity.getSignDuration() * 60 * 1000);
        entity.setModifyId(ctx.getAccountId());
        entity.setModifyName(ctx.getAccountName());
        entity.setModifyTime(new Date());
        sysAppTokenServiceImpl.update(entity);
        return responseDto;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_APP_TOKEN,businessName="删除",operationType= OperationLogEnum.Delete)
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public WebResponseDto delete(@PathVariable Long id){
        WebResponseDto responseDto = new WebResponseDto();
        sysAppTokenServiceImpl.delete(id);
        return responseDto;
    }
}

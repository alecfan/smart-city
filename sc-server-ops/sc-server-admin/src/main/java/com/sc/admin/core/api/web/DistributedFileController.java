package com.sc.admin.core.api.web;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.admin.core.service.SysDistributedFileService;
import com.sc.common.annotations.WebApi;
import com.sc.common.dto.PageDto;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import com.sc.common.entity.admin.distributedfile.SysDistributedFileSearch;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-03-08 15:25:30
 * @description:
 */
@WebApi
@RequestMapping("/web/DistributedFileController")
@RestController
public class DistributedFileController {
    @Autowired
    private SysDistributedFileService sysDistributedFileServiceImpl;

   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody SysDistributedFileSearch search){
       WebResponseDto responseDto = new WebResponseDto();

       PageDto pageDto = search.getPageDto();
       Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());
       List<SysDistributedFile> list =  sysDistributedFileServiceImpl.select(search);
       responseDto.setLstDto(list);
       BeanUtils.copyProperties(page,pageDto);
       responseDto.setPage(pageDto);
       return responseDto;
   }


   @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
   public WebResponseDto delete(@PathVariable Long id){
       WebResponseDto responseDto = new WebResponseDto();
       sysDistributedFileServiceImpl.deleteByPrimaryKey(id);
       return responseDto;
   }
}

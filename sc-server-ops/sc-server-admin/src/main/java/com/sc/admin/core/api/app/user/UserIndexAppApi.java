/**
 * Created by wust on 2019-12-19 10:42:49
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.app.user;


import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.util.cache.SpringRedisTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

/**
 * @author: wust
 * @date: Created in 2019-12-19 10:42:49
 * @description: app首页的一些公共操作
 *
 */
@Api(tags = {"App接口~员工首页"})
@RequestMapping("/api/app/v1/UserIndexAppApi")
@RestController
public class UserIndexAppApi {
    @Autowired
    private SpringRedisTools springRedisTools;

    /**
     * 切换项目，将当前选中的项目和项目所属的公司绑定给当前用户，并放入缓存
     * @param projectId
     * @return
     */
    @ApiOperation(value = "切换项目", httpMethod = "POST")
    @RequestMapping(value = "/switchProject/{projectId}",method = RequestMethod.POST)
    public WebResponseDto switchProject(@PathVariable Long projectId){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        String key = ctx.getRedisKey();
        Map map = springRedisTools.getMap(key);
        if(map != null){
             // TODO 根据项目id去相关关系表获取公司和代理商
            springRedisTools.addFieldToMap(key,"projectId",projectId);
            springRedisTools.addFieldToMap(key,"companyId",1);
            springRedisTools.addFieldToMap(key,"agentId",1);
        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("请先登录系统");
        }
        return responseDto;
    }
}

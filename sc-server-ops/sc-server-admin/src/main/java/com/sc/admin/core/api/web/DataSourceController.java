package com.sc.admin.core.api.web;

import com.sc.admin.core.service.SysDataSourceService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.datasource.SysDataSource;
import com.sc.common.entity.admin.datasource.SysDataSourceSearch;
import com.sc.common.enums.OperationLogEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;

/**
 * Created by wust on 2019/6/17.
 */
@WebApi
@RequestMapping("/web/DataSourceController")
@RestController
public class DataSourceController {
    @Autowired
    private SysDataSourceService sysDataSourceService;

    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_DATA_SOURCE,businessName="分页查询",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysDataSourceSearch search){
        WebResponseDto baseDto = new WebResponseDto();
        return baseDto;
    }



    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_DATA_SOURCE,businessName="修改",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "",method = RequestMethod.PUT)
    public WebResponseDto update(@RequestBody SysDataSource entity){
        WebResponseDto mm = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        entity.setModifyId(ctx.getAccountId());
        entity.setModifyName(ctx.getAccountName());
        entity.setModifyTime(new Date());
        sysDataSourceService.updateByPrimaryKeySelective(entity);
        return mm;
    }
}

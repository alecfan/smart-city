-- 导出  表 sc-admin.flyway_schema_history 结构
CREATE TABLE IF NOT EXISTS `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `script` varchar(1000) COLLATE utf8mb4_general_ci NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_account 结构
CREATE TABLE IF NOT EXISTS `sys_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '该id来自员工和客户表的ID,方便查找',
  `account_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `account_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号名称',
  `password` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号密码',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号类型：管理员，员工，客户',
  `email` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电子邮件',
  `contact_number` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `source` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '注册来源：web注册，app注册',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243433795332341761 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='账号中心表';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_account_resource 结构
CREATE TABLE IF NOT EXISTS `sys_account_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `resource_code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique` (`account_id`,`resource_code`,`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243436226187362312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='账号资源表';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_alarm_record 结构
CREATE TABLE IF NOT EXISTS `sys_notification_timeline` (
    `id` bigint(0) NOT NULL AUTO_INCREMENT,
    `notification_id` bigint(0) NOT NULL COMMENT '消息id',
    `relation_code` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '唯一关系码',
    `type` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '节点类型：发出警报/解除警报',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
    `creater_id` bigint(0) NULL DEFAULT NULL,
    `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建用户名称',
    `modify_id` bigint(0) NULL DEFAULT NULL,
    `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新用户名称',
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '最后更新时间',
    `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='通知时间线';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_app_token 结构
CREATE TABLE IF NOT EXISTS `sys_app_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'app标识',
  `app_expire_time` datetime DEFAULT NULL COMMENT 'app过期时间，过期后该appid用户不能访问系统任何接口',
  `sign_secret_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '加密的秘钥',
  `sign_duration` bigint(20) DEFAULT NULL COMMENT '签名保留时长，存储单位为毫秒，存储格式：分钟数*60秒*1000毫秒',
  `applicant` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '申请者',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态：新申请、申请通过、申请不通过',
  `agent_id` bigint(20) DEFAULT NULL,
  `parent_company_id` bigint(20) DEFAULT NULL,
  `branch_company_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL COMMENT '所属代理商',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_id` (`id`) USING BTREE,
  UNIQUE KEY `idx_uniqe_app_id` (`app_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243433018429800449 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='应用程序访问系统管理';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_blacklist 结构
CREATE TABLE IF NOT EXISTS `sys_blacklist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='黑名单管理';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_company 结构
CREATE TABLE IF NOT EXISTS `sys_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `leader` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司负责人',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `admin_account` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '管理员登录账号',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243432573971988481 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_customer 结构
CREATE TABLE IF NOT EXISTS `sys_customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登录账号',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '客户名称',
  `sex` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '性别',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '客户类型：个人客户，客户',
  `apply_status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '申请状态：新注册，审核通过，审核不通过',
  `mobile_phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '移动电话',
  `telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电子邮箱',
  `ID_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份号：个人客户使用身份证号，企业客户使用企业资质号',
  `status` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '启用，禁用',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `agent_id` bigint(20) DEFAULT NULL,
  `parent_company_id` bigint(20) DEFAULT NULL,
  `branch_company_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;


CREATE TABLE IF NOT EXISTS `sys_data_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL COMMENT '数据源名称：取数据库名称' COLLATE 'utf8mb4_general_ci',
  `group_name` VARCHAR(36) NULL DEFAULT NULL COMMENT '组是以默认数业务数据库命名，组名称+代理商ID能找到代理商的数据库组' COLLATE 'utf8mb4_general_ci',
  `master_slave` VARCHAR(10) NULL DEFAULT NULL COMMENT '主从库标志，用于后期实现读写分离用' COLLATE 'utf8mb4_general_ci',
  `db_type` VARCHAR(15) NULL DEFAULT NULL COMMENT '数据源类型' COLLATE 'utf8mb4_general_ci',
  `jdbc_url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
  `jdbc_username` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
  `jdbc_password` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
  `jdbc_driver` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
  `description` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
  `sharding_column` VARCHAR(255) NULL DEFAULT NULL COMMENT '分库列' COLLATE 'utf8mb4_general_ci',
  `project_id` BIGINT(20) NULL DEFAULT NULL,
  `creater_id` BIGINT(20) NULL DEFAULT NULL,
  `creater_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
  `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
  `modify_id` BIGINT(20) NULL DEFAULT NULL,
  `modify_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
  `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `sys_data_source_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_source_id` bigint(20) NOT NULL COMMENT '数据源ID',
  `actual_table` varchar(50) DEFAULT NULL COMMENT '物理表名',
  `logical_table` varchar(50) DEFAULT NULL COMMENT '逻辑表名',
  `sharding_column` varchar(50) DEFAULT NULL COMMENT '分表列',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;


-- 导出  表 sc-admin.sys_department 结构
CREATE TABLE IF NOT EXISTS `sys_department` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `leader` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门负责人',
  `type` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门类型',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `parent_company_id` bigint(20) DEFAULT NULL,
  `branch_company_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243433436094398465 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_distributed_file 结构
CREATE TABLE IF NOT EXISTS `sys_distributed_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '该id来自员工和客户表的ID,方便查找',
  `bucket_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Name of the bucket.',
  `object_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Object name in the bucket.',
  `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件名',
  `size` bigint(20) DEFAULT '0' COMMENT '文件大小',
  `file_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件类型',
  `source` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '来源：用“数据库名.表名”做为标记',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243430627353559041 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='分布式文件存储记录表';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_import_export 结构
CREATE TABLE IF NOT EXISTS `sys_import_export` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `module_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operation_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `size` int(11) DEFAULT NULL,
  `file_id` bigint(20) DEFAULT NULL COMMENT '分布式文件对应id',
  `creater_id` bigint(20) DEFAULT NULL COMMENT '创建人id',
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL COMMENT '更新人id',
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_batch_no` (`batch_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243430626632138753 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_lookup 结构
CREATE TABLE IF NOT EXISTS `sys_lookup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `parent_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `root_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lan` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `visible` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `company_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_code` (`code`,`lan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1246774669839695873 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_menu 结构
CREATE TABLE IF NOT EXISTS `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pcode` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `permission` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL COMMENT '菜单层级 , 在同一个pid下 , 值越小层级越高',
  `sort` bigint(20) DEFAULT NULL COMMENT '菜单排序 , 在同一个pid下可重新排序',
  `img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` varchar(135) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单类型',
  `visible` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否可见',
  `is_parent` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_type` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户类型',
  `lan` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '语言',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_code` (`code`,`user_type`,`lan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1246774671890710530 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_my_notice 结构
CREATE TABLE IF NOT EXISTS `sys_my_notice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_id` bigint(20) DEFAULT NULL COMMENT '发布通知表sys_notification的id',
  `receiver` bigint(20) DEFAULT NULL COMMENT '接收用户id',
  `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态：未读、已读',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_id` (`id`) USING BTREE,
  KEY `idx_normal_notification_id` (`notification_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243430004298088467 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='我收到的通知记录表';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_notification 结构
CREATE TABLE IF NOT EXISTS `sys_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '通知标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '通知内容',
  `receiver_type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接收者类型：系统所有用户、指定公司所有用户、指定项目所有用户、指定用户',
  `receiver` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接收者id集合',
  `receiver_label` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接收者名称集合',
  `type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消息类型：通知消息、警报消息',
  `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态：草稿、生效中、已失效',
  `priority_level` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公告优先级别：紧急、重要、一般',
  `send_channel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '发送渠道：web浏览器、邮件、短信、公众号',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243429965345587201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='发布通知记录表';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_organization 结构
CREATE TABLE IF NOT EXISTS `sys_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(8) DEFAULT NULL,
  `type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `relation_id` bigint(8) DEFAULT NULL,
  `creater_id` bigint(8) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(8) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243434720432226305 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_project 结构
CREATE TABLE IF NOT EXISTS `sys_project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编码',
  `name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '项目名',
  `addr` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '项目地址',
  `description` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '项目描述',
  `admin_account` varchar(50) DEFAULT NULL COMMENT '管理员账号',
  `agent_id` bigint(20) DEFAULT NULL,
  `parent_company_id` bigint(20) DEFAULT NULL,
  `branch_company_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '创建用户名称',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '最后更新用户名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243433018262028289 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='项目表';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_resource 结构
CREATE TABLE IF NOT EXISTS `sys_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `menu_code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `permission` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `request_method` varchar(6) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_type` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号类型',
  `lan` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '语言',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_uniqu_code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1246774672742154241 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_role 结构
CREATE TABLE IF NOT EXISTS `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `parent_company_id` bigint(20) DEFAULT NULL,
  `branch_company_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_unique_code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243433494407806977 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_role_resource 结构
CREATE TABLE IF NOT EXISTS `sys_role_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organization_id` bigint(20) DEFAULT NULL COMMENT '组织id',
  `resource_code` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique` (`organization_id`,`resource_code`,`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1245988147909623809 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_shortcut_menu 结构
CREATE TABLE IF NOT EXISTS `sys_shortcut_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `menu_code` varchar(36) COLLATE utf8mb4_general_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1228990198986047489 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户的快捷菜单访问配置表';

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_user 结构
CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登录账号',
  `real_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '昵称',
  `sex` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '性别',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱地址',
  `mobile_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号码',
  `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '启用，禁用',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号类型：管理员、代理商、总公司、分公司、项目、操作员',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `agent_id` bigint(20) DEFAULT NULL,
  `parent_company_id` bigint(20) DEFAULT NULL,
  `branch_company_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_login_name` (`login_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1243433795332341761 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

-- 导出  表 sc-admin.sys_user_organization 结构
CREATE TABLE IF NOT EXISTS `sys_user_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `branch_company_id` bigint(20) DEFAULT NULL,
  `parent_company_id` bigint(20) DEFAULT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1246774689003470849 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- 数据导出被取消选择。

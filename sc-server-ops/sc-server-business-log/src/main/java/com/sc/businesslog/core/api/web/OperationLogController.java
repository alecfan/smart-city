package com.sc.businesslog.core.api.web;


import com.github.pagehelper.PageInfo;
import com.sc.businesslog.core.service.BlOperationLogService;
import com.sc.businesslog.entity.BlOperationLogList;
import com.sc.businesslog.entity.BlOperationLogSearch;
import com.sc.common.annotations.WebApi;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * Created by wust on 2019/5/28.
 */
@WebApi
@RequestMapping("/web/OperationLogController")
@RestController
public class OperationLogController {
    @Autowired
    private BlOperationLogService blOperationLogServiceImpl;

    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody BlOperationLogSearch search){
        WebResponseDto responseDto = new WebResponseDto();

        List<BlOperationLogList> list =  blOperationLogServiceImpl.selectByPageNumSize(search,search.getPageDto().getPageNum(),search.getPageDto().getPageSize()) == null ? null :
                (List<BlOperationLogList>)blOperationLogServiceImpl.selectByPageNumSize(search,search.getPageDto().getPageNum(),search.getPageDto().getPageSize());
        PageInfo page = new PageInfo(list);
        PageDto pageDto = new PageDto();
        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        responseDto.setLstDto(list);
        return responseDto;
    }
}

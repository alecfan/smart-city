# 说明
    - 本套系统与市面上面向大众的基础服务框架有所不同：
        - 1. 支持多租户
        - 2. 支持分库分表
        - 3. 结合mq实现分布式部署下缓存与数据库的同步功能
        - 4. 代码结构简单清晰，模块划分合理。后期可轻松进行扩展与维护。
        - 5. 实现了功能权限控制、数据字典配置、接口鉴权、定时任务、接口文档聚合、服务监控、链路跟踪、业务日志和系统日志等待功能。
# 技术概览
    -- 存储技术
         MySQL 8+
         Redis
         Ehcache
         Minio
         RabbitMQ
    -- 后端相关基础
        SpringCloud
        Spring Quartz
        shardingsphere
        JDK 1.8+
        Maven
        Flyway
    -- 前端相关技术知识
        Vue
        Element UI
        zTree
        Webpack
    -- 分布式监控相关
        ELK
        链路监控
        服务健康检查
# 工程结构
     
     |——smart-city
     |——————sc-helper                          -- 辅助模块
     |————————sc-helper-common                   -- 公共辅助模块，存放高度共享和高度抽象的代码
     |————————sc-helper-ds                       -- 数据源公共配置模块
     |————————sc-helper-easycode                 -- 代码生成辅助模块
     |————————sc-helper-easyexcel                -- 处理excel的模块，能够简化excel的解析、校验与生成
     |————————sc-helper-feign                    -- feign公共配置模块
     |————————sc-helper-hystrix                  -- hystrix公共配置模块
     |————————sc-helper-log-biz                  -- 业务日志公告配置模块
     |————————sc-helper-minio                    -- minio公告配置模块
     |————————sc-helper-mq                       -- mq公共配置模块
     |————————sc-helper-redis                    -- redis公共配置模块
     |——sc-server-biz                          -- 业务服务模块
     |————————sc-server-demo1                    -- demo1服务（使用shardingjdbc分库分表）
     |————————sc-server-demo2                    -- demo2服务（不分库分表）
     |————————sc-server-work-order               -- 工单服务
     |——— sc-server-biz-api                    -- 业务服务提供出去的api模块
     |————————sc-server-demo1-api                -- demo1服务api
     |————————sc-server-demo2-api                -- demo2服务api
     |————————sc-server-work-order-api           -- 工单服务api
     |——sc-server-ops                          -- 运维模块
     |————————sc-server-admin                    -- 研发管理/公共管理服务
     |————————sc-server-autotask                 -- 自动作业服务 
     |————————sc-server-business-log             -- 业务日志服务 
     |————————sc-server-config                   -- 配置服务 
     |————————sc-server-eureka                   -- 注册中心服务
     |————————sc-server-gateway                  -- 网关服务
     |————————sc-server-monitor                  -- 监控服务
     |——sc-server-ops-api                      -- 运维模块提供出去的api
     |————————sc-server-admin-api                -- 管理服务api
     |————————sc-server-autotask-api             -- 自动作业服务api
     |————————sc-server-business-log-api         -- 业务日志服务api
# 1. 开发环境搭建（window 64）
    1）安装JDK；
        - 当前使用版本：1.8.x
        - 下载地址：https://www.oracle.com/java/technologies/javase-jdk14-downloads.html
        - 配置环境变量（安装请自行谷歌百度）
    2）安装工程管理组件Maven；
        - 当前使用版本：3.6.3
        - 下载地址：http://maven.apache.org/download.cgi
        - 配置环境变量（安装请自行谷歌百度）
    3）安装Redis；
        - 当前使用版本：3.2
        - windows版下载地址：https://github.com/microsoftarchive/redis/releases
        - 全局搜索并修改工程配置，如下：
             redis:
                 host: localhost
                 port: 6379
        - 启动
            cd D:/workspace/redis-3.2/
            redis-server.exe redis.windows.conf
    4）安装Minio；
         - 下载地址：https://min.io/download
         - 全局搜索并修改工程配置，如下：
             minio:
                url: http://localhost:9000
                access-key: minioadmin
                secret-key: minioadmin
                bucket-name: demo1server
         - 启动
            cd D:/workspace/minio/
            minio.exe server /data
         - 控制台：http://localhost:9000
    5）安装RabbitMq，开启web插件服务，修改配置文件为你的地址（https://www.rabbitmq.com/）；
        - 当前使用版本：3.8.2
        - 下载地址：https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.5/rabbitmq-server-3.8.5.exe
        - 安装（请自行谷歌百度）
        - 启动
            cd D:\Program Files\RabbitMQ Server\rabbitmq_server-3.8.2\sbin
            rabbitmq-server -detached
        - 开启web插件
            cd D:\Program Files\RabbitMQ Server\rabbitmq_server-3.8.2\sbin
            rabbitmq-plugins enable rabbitmq_management
        - 开启web_stomp插件
            cd D:\Program Files\RabbitMQ Server\rabbitmq_server-3.8.2\sbin
            rabbitmq-plugins enable rabbitmq_web_stomp
        - 登录控制台并建立相关配置：
            控制台地址：http://localhost:15672  
            默认账号密码：guest/guest  
            新建账号
            新建Virtual host（开发环境新建名称为dev的vhost，测试环境新建名称为test的vhost，生产环境新建名称为prod的vhost）
            给新建的账号授予访问dev这个vhost的权限
        - 全局搜索并修改工程配置，如下：
            spring:
                rabbitmq:
                    host: localhost
                    port: 5672
                    username: ${你的账号}
                    password: ${你的密码}
    6）安装mysql服务（8.0及以上版本）；
        - 安装请自行谷歌百度
        - 全局搜索并修改工程配置，如下：
            spring:
                datasource:
                    druid:
                      url: jdbc:mysql://localhost:3306/${db-default-name}?useSSL=false&allowPublicKeyRetrieval=true&allowMultiQueries=true&serverTimezone=GMT%2B8
                      username: root
                      password: 123456
    7）新建空数据库（目前只有在使用了shardingjdbc组件的服务需手动提前建立好数据库且不需要手工建立表，其他服务对应的数据库会在系统启动时自动建立好）；
        sc-business-log0-master0
        sc-demo10-master0
        sc-demo11-master0
        sc-demo12-master0
        sc-demo13-master0
        sc-demo14-master0
        sc-demo15-master0
    8）可选安装ELK（由于这些组件对系统性能要求比较高，本地环境或者非集群环境可以先忽略）：
        https://www.elastic.co/cn/downloads/elasticsearch
        https://github.com/mobz/elasticsearch-head
        https://www.elastic.co/cn/downloads/kibana
        https://www.elastic.co/cn/downloads/logstash
    9）依次启动服务
        - 必须要启动的服务
            sc-server-eureka
            sc-server-admin
            业务服务
            sc-server-gateway（该服务尽量留在最后启动）
        - 可选启动业务服务（根据开发需要选择启动）
            sc-server-autotask
            sc-server-business-log
            sc-server-demo1
            sc-server-demo2
            sc-server-work-order
        - 可选启动监控服务（生产环境建议启动）    
            sc-server-monitor
            zipkin-server-2.21.4-exec.jar（官方文档https://zipkin.io/pages/quickstart.html）
    10）启动前端vue（配置和启动步骤参见vue项目根目录的README.md）
    11）访问地址：
        - 研发管理主后台：http://localhost:8080 admin/admin123
        - 服务监控地址：http://localhost:8782/monitor-server
        - 链路监控地址：http://localhost:9411/zipkin
        - 文档聚合地址：http://localhost:8773/swagger-ui.html
        - elasticsearch：http://localhost:9200
        - elasticsearch-head：http://localhost:9100
        - kibana：http://localhost:5601
        - logstash：http://localhost:5044
        - RabbitMq控制台：http://localhost:15672
        - minio：http://localhost:9000
# 2. 开启本地缓存（根据ID主键缓存持久化对象）
    1）步骤描述
        在持久对象上面增加注解@EnableLocalCaching即可
    2）示例代码
        @EnableLocalCaching
        public class SysAccount extends BaseEntity {
# 3. 开启分布式缓存（根据ID主键缓存持久化对象）
    1）步骤描述
        在持久对象上面增加注解@EnableDistributedCaching即可
    2）示例代码
        @EnableDistributedCaching
        public class SysAccount extends BaseEntity {
# 4. 自定义缓存（复杂key）
    本地缓存和分布式缓存只解决了持久化对象根据关键字缓存数据功能。如果需要构建某持久化对象的复杂key缓存，则需要自定义缓存。
    1）步骤描述
        在此包[com.*.admin.core.cache]下面添加一个BO类，该类继承CacheAbstract类，然后实现相关方法即可
    2）示例代码
        请参考com.*.admin.core.cache.RedisCacheAccountBo类的实现
# 5. 系统启动，执行相关初始化业务
     系统启动的时候通常会需要执行一些初始化业务，此时可以根据下面的配置来做即可
     1）步骤描述
        - 增加接口InitializtionService的实现类
        - 如果需要编排初始化舒徐，可以在实现类添加类注解@Order(0)来定义初始化类的执行顺序
     2）示例代码
        请参考com.*.admin.core.service.impl.InitData4AccountServiceImpl类的实现
# 6. 菜单资源配置
    目前支持六种账号类型的菜单资源视图配置，菜单配置支持国际化，并且可以根据自己的业务自由扩展菜单视图。
    1）subsystem-A100401.xml（系统管理员账号菜单视图配置）
        xml元素说明
            modulegroup - 菜单组
            module - 菜单
            operation - 资源
        xml元素属性说明
            code - 使用UUID，同一个文件内保证唯一即可，不同文件不要求唯一
            name - 菜单名称
            desc - 菜单描述
            order - 菜单排序
            img - 菜单图片
            permission - 权限标识符
            url - 菜单路由地址/资源的接口地址
        示例代码
            <!-- 没有菜单组的配置 -->
            <module code="270a90e2-349b-48ba-b7c6-d295e6700243" name="Customer Management" desc="用户中心" order="100050313" page="CustomerList" url="./CustomerList" img="/static/icon/menu/用户管理.png">
                <operation code="dd721b7f-6331-4969-bc36-206d3f16ec97" name="Search" desc="查询" url="" requestMethod="POST"/>
                <operation code="5d876d42-bbbf-48fa-a0b4-48d9dda6198e" name="Export" desc="导出" url="/web/ExportExcelController/exportExcel"/>
                <operation code="ba7c9717-9210-4f36-99cb-8fdf268fc803" name="Update" desc="修改" url="" requestMethod="PUT"/>
            </module>
            <!-- 有菜单组的配置 -->
            <modulegroup code="475b0298-2a39-488f-86cf-6d9b6ce35c34" name="Organization Management" desc="组织管理" order="1000503"  img="/static/icon/menu/组织架构.png">
                <module code="270a90e2-349b-48ba-b7c6-d295e6700243" name="Customer Management" desc="用户中心" order="100050313" page="CustomerList" url="./CustomerList" img="/static/icon/menu/用户管理.png">
                    <operation code="dd721b7f-6331-4969-bc36-206d3f16ec97" name="Search" desc="查询" url="" requestMethod="POST"/>
                    <operation code="5d876d42-bbbf-48fa-a0b4-48d9dda6198e" name="Export" desc="导出" url="/web/ExportExcelController/exportExcel"/>
                    <operation code="ba7c9717-9210-4f36-99cb-8fdf268fc803" name="Update" desc="修改" url="" requestMethod="PUT"/>
                </module>
            </modulegroup>
    2）subsystem-A100403.xml（代理商管理员账号菜单视图配置）
        同subsystem-A100401.xml
    3）subsystem-A100406.xml（总公司管理员账号菜单视图配置）
        同subsystem-A100401.xml
    4）subsystem-A100409.xml（分公司管理员账号菜单视图配置）
        同subsystem-A100401.xml
    5）subsystem-A100410.xml（项目管理员账号菜单视图配置）
        同subsystem-A100401.xml
    6）subsystem-A100411.xml（普通业务员菜单视图配置）
        同subsystem-A100401.xml
# 7. 数据字典配置
    数据字典支持国际化配置，如果需要国际化配置，只需要配置好默认文件然后复制一份并命名为相关语言的xml即可，如lookup.xml->lookup_en_US.xml
    xml元素说明
        RECORD - 记录元素，对应数据库一行记录
    xml元素属性说明
        code - 该属性值要求全局唯一，并且命名具有规则性，如高度公共的数据字典使用A开头的code，相对私有的数据字典使用其他字母开头
        parent_code - 父code，根节点则该属性值为空字符
        root_code - 根code，当需要配置多级数据字典的时候，该属性尤其重要
        name - 数据字典名称
        description - 数据字典描述
        value - 自定义值，备用字段
        sort - 排序
    示例代码
        <!-- 没有级联的数据字典 -->
        <RECORD code="A1003" parent_code="" name="性别" description="性别" sort="0"/>
        <RECORD code="A100301" parent_code="A1003" root_code="A1003" value="M" name="男" description="男" sort="0"/>
        <RECORD code="A100302" parent_code="A1003" root_code="A1003" value="F" name="女" description="女" sort="0"/>

        <!-- 有级联的数据字典 -->
        <RECORD code="A1005" parent_code="" name="省" description="省" sort="0"/>
        <RECORD code="A100501" parent_code="A1005" root_code="A1005" value="" name="广西省" description="广西省" sort="0"/>
        <RECORD code="A10050101" parent_code="A100501" root_code="A1005" value="" name="南宁市" description="南宁市" sort="0"/>
# 8. RabbitMQ的使用
    生产者
        在包[com.*.admin.core.mq.producer]下面有三种交换机，其中直连型的交换机已经实现，其他两种交换机可根据后期业务需求扩展即可。
    消费者
        在包[com.*.admin.core.mq.consumer]下面增加队列监听，即可实现队列的消费。
    1）步骤描述
        - 在sc-helper-common模块下面的文件[application-rabbitmq.yml]添加队列和交换机配置
        - 然后通过[com.*.admin.core.mq.producer]下面的生产者调用send方法进行发送消息
    2）示例代码
        参考Consumer4PublishNotificationQueue类的实现
# 9. 导入导出（Excel）
    本系统实现了有规则数据的导入导出功能，导出只需要配置XML文件即可快速实现导出Excel功能，导入也只需要配置XML即可实现对Excel的快速验证、解析等。
    痛点与难点
        - 如何抽象封装出Excel的解析、验证过程
        - 如何抽象封装出将数据写入Excel的过程
        - 分布式环境下文件的存储
        - 多用批量导入如何防止数据库压力暴增（排队）
        - 如何实现导出大Excel文件
    导入excel的实现
        - 请参考UserController.importByExcel
    导出excel的实现
        - 请参考ExportExcelController.exportExcel的实现
# 10. 接口文档
    1）描述
        接口文档采用swagger2组件进行文档聚合，使用方式请参考官方文档
    2）官方文档
        https://swagger.io/docs
# 11. 为接口添加业务日志
    1）步骤描述
        在需要记录业务日志的接口方法添加注解@OperationLog
    2）示例代码
        @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_COMPANY,businessName="分页查询",operationType= OperationLogEnum.Search)
        public WebResponseDto listPage(@RequestBody SysCompanySearch search){
# 12. 数据库升级
    1）步骤描述
        - 数据库升级管理采用flyway组件，在每个微服务的resources/db/migration目录下面有V开头的SQL文件，升级数据库只需要建立新版本的SQL文件即可，重启服务后所有数据源对应的数据库都会被升级
        - SQL文件名规则：V${年}${月}${日}${时}${分}.sql
        - 注意：不用在升级脚本里面出现drop数据库和drop表操作，否则可能会造成生成环境数据丢失（请三思而后行），更多使用规则请参考官方文档
    2）官方文档
        https://flywaydb.org
# 13. 运维文档
    请参考smart-city/doc下面的相关文档
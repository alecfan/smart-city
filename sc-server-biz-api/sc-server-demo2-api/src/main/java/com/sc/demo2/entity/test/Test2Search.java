package com.sc.demo2.entity.test;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-07-04 13:29:42
 * @description:
 */
public class Test2Search extends Test2 {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
package com.sc.demo1.entity.demo;

import com.sc.common.dto.PageDto;

/**
 * @author ：wust
 * @date ：Created in 2019/9/9 11:19
 * @description：
 * @version:
 */
public class DemoSearch extends Demo {
    private static final long serialVersionUID = -5945492370688862566L;

    private PageDto pageDto;

    private String areaType;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }

    public String getAreaType() {
        return areaType;
    }

    public void setAreaType(String areaType) {
        this.areaType = areaType;
    }
}

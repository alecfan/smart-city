package com.sc.generatorcode.task;

import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：wust
 * Date   2020/06/30
 */
public class ImportServiceTask extends AbstractTask {

    public ImportServiceTask(String className) {
        super(className);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];

        Map<String, String> data = new HashMap<>();
        data.put("BasePackageName", ConfigUtil.getConfiguration().getBasePackage().getBase());
        data.put("InterfacePackageName", ConfigUtil.getConfiguration().getBasePackage().getInterf());
        data.put("Author", ConfigUtil.getConfiguration().getAuthor());
        data.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        data.put("ClassName", className);

        String filePath = FileUtil.getSourcePath() + StringUtil.package2Path(ConfigUtil.getConfiguration().getBasePackage().getInterf());
        String fileName = className + "ImportService.java";
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_IMPORT_SERVICE, data, filePath,fileName);
    }
}

package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class MenuTask extends AbstractTask {

    public MenuTask(String className, List<ColumnInfo> infos) {
        super(className,infos);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];

        String rootDir = System.getProperty("user.dir");
        String menuDir = rootDir + "\\gen";
        FileUtil.mkdirs(menuDir);
        String fileName = "menu.txt";

        try {
            StringBuffer content = new StringBuffer("<module code=\""+ UUID.randomUUID().toString() +"\" name=\""+ StringUtil.firstToUpperCase(name) + "\" desc=\"菜单名称\" order=\"0\" page=\""+ StringUtil.firstToUpperCase(name) +"List\" url=\"./"+ StringUtil.firstToUpperCase(name) +"List\" img=\"/static/icon/menu/临时图片.png\">");
            content.append("\n");
            content.append("\t<operation code=\""+UUID.randomUUID().toString()+"\" name=\"Search\" desc=\"查询\" url=\"\" requestMethod=\"POST\"/>");
            content.append("\n");
            content.append("\t<operation code=\""+UUID.randomUUID().toString()+"\" name=\"Create\" desc=\"新建\" url=\"\" requestMethod=\"POST\"/>");
            content.append("\n");
            content.append("\t<operation code=\""+UUID.randomUUID().toString()+"\" name=\"Update\" desc=\"修改\" url=\"\" requestMethod=\"PUT\"/>");
            content.append("\n");
            content.append("\t<operation code=\""+UUID.randomUUID().toString()+"\" name=\"Delete\" desc=\"删除\" url=\"\" requestMethod=\"DELETE\"/>");
            content.append("\n");
            content.append("\t<operation code=\""+UUID.randomUUID().toString()+"\" name=\"Export\" desc=\"导出\" url=\"/web/ExportExcelController/exportExcel\" requestMethod=\"POST\"/>");
            content.append("\n");
            content.append("\t<operation code=\""+UUID.randomUUID().toString()+"\" name=\"Import\" desc=\"导入\" url=\"\" requestMethod=\"POST\"/>");
            content.append("\n");
            content.append("</module>");

            File file = new File(menuDir);
            if(!file.exists()){
                file.mkdirs();
            }
            file = new File(menuDir + File.separator + fileName);
            file.createNewFile();
            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            out.write(content.toString());
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package ${EntityPackageName};

import ${BasePackageName}.common.entity.BaseEntity;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
public class ${ClassName} extends BaseEntity {
    ${Properties}

    ${Methods}
}
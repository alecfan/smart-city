/**
* Created by ${Author} on ${DateTime}.
*/
<template>
  <el-form ref="formModel" :model="formModel" :rules="rules" label-width="120px" style="width:70%;" @submit.native.prevent>
      ${formItem}
      <el-form-item style="text-align: left;">
        <el-button type="primary" @click="onSubmit('formModel')" :loading="submitting">提交</el-button>
      </el-form-item>
  </el-form>
</template>
<script>
import Vue from 'vue'

export default {
  name: '${name}-update',
  props: ['selectedModel'],
  data () {
    return {
      submitting: false,
      formModel: {
        ${formModelProperty}
      },
      rules: {
      }
    }
  },
  created: function () {
    this.formModel = this.selectedModel
  },
  methods: {
    onSubmit: function (formData) {
      this.$refs[formData].validate((valid) => {
        if (!valid) {
        } else {
          Vue.$ajax({
            method: 'put',
            url: ${axiosReqPrefix} + '/web/${moduleName}Controller',
            data: this.formModel
          }).then(res => {
            if (res.data.flag !== 'SUCCESS') {
              if (!Vue.$isNullOrIsBlankOrIsUndefined(res.data.message)) {
                this.$message({
                  message: res.data.message,
                  type: 'warning'
                })
              }
            } else {
              this.$message({
                message: res.data.message,
                type: 'success'
              })
            }
          })
        }
      })
    }
  }
}
</script>

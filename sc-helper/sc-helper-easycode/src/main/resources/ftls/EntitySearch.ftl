package ${EntityPackageName};

import ${BasePackageName}.common.dto.PageDto;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
public class ${ClassName}Search extends ${ClassName} {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
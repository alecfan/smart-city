package com.sc.common.entity.admin.userorganization;

/**
 * @author ：wust
 * @date ：Created in 2019/8/9 9:57
 * @description：
 * @version:
 */
public class SysUserOrganizationList extends SysUserOrganization {
    private static final long serialVersionUID = -7427038018675160525L;

    private String branchCompanyName;

    private String projectName;

    private String departmentName;

    private String roleName;

    public String getBranchCompanyName() {
        return branchCompanyName;
    }

    public void setBranchCompanyName(String branchCompanyName) {
        this.branchCompanyName = branchCompanyName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}

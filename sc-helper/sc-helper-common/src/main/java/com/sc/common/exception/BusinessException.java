package com.sc.common.exception;

import com.sc.common.enums.ReturnCode;

/**
 * Created by wust on 2019/4/28.
 */
public class BusinessException extends RuntimeException implements ReturnCodeAware{

    private String code;
    private String message;
    private Object[] args;


    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message) {
        super(message);
        this.message = message;
    }

    public BusinessException(String message,Throwable cause) {
        super(message,cause);
        this.message = message;
    }

    public BusinessException(ReturnCode returnCode) {
        super(returnCode.getMessage());
        this.code = returnCode.getCode();
        this.message = returnCode.getMessage();
    }

    public BusinessException(ReturnCode returnCode, Throwable cause) {
        super(returnCode.getMessage(),cause);
        this.code = returnCode.getCode();
        this.message = returnCode.getMessage();
    }

    public BusinessException(ReturnCode returnCode, String message, Throwable cause) {
        super(message,cause);
        this.code = returnCode.getCode();
        this.message = message;
    }


    @Override
    public String getErrorCode() {
        return code;
    }

    @Override
    public String getErrorMessage() {
        return message;
    }

    @Override
    public Object[] getArgs() {
        return args;
    }
}

package com.sc.common.entity.admin.notification;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-04-09 13:26:08
 * @description:
 */
public class SysNotificationTimelineSearch extends SysNotificationTimeline {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
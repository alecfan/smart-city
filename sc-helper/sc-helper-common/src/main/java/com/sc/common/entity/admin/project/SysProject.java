package com.sc.common.entity.admin.project;

import com.sc.common.annotations.EnableDistributedCaching;
import com.sc.common.annotations.EnableLocalCaching;
import com.sc.common.entity.BaseEntity;


/**
 * @author ：wust
 * @date ：Created in 2019/7/30 11:39
 * @description：
 * @version:
 */
@EnableLocalCaching
@EnableDistributedCaching
public class SysProject extends BaseEntity {
    private static final long serialVersionUID = 869596893803958296L;

    private String code;
    private String name;
    private String addr;
    private String description;
    private String adminAccount;
    private Long agentId;
    private Long parentCompanyId;
    private Long branchCompanyId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdminAccount() {
        return adminAccount;
    }

    public void setAdminAccount(String adminAccount) {
        this.adminAccount = adminAccount;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getParentCompanyId() {
        return parentCompanyId;
    }

    public void setParentCompanyId(Long parentCompanyId) {
        this.parentCompanyId = parentCompanyId;
    }

    public Long getBranchCompanyId() {
        return branchCompanyId;
    }

    public void setBranchCompanyId(Long branchCompanyId) {
        this.branchCompanyId = branchCompanyId;
    }

    @Override
    public String toString() {
        return "SysProject{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", addr='" + addr + '\'' +
                ", description='" + description + '\'' +
                ", adminAccount='" + adminAccount + '\'' +
                ", agentId=" + agentId +
                ", parentCompanyId=" + parentCompanyId +
                ", branchCompanyId=" + branchCompanyId +
                '}';
    }
}

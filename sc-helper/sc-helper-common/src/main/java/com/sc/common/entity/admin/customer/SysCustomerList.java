package com.sc.common.entity.admin.customer;


/**
 * @author: wust
 * @date: 2019-12-27 11:37:51
 * @description:
 */
public class SysCustomerList extends SysCustomer {
    private String sexLabel;
    private String applyStatusLabel;
    private String statusLabel;
    private String typeLabel;

    private String agentName;
    private String parentCompanyName;
    private String branchCompanyName;
    private String projectName;

    public String getSexLabel() {
        return sexLabel;
    }

    public void setSexLabel(String sexLabel) {
        this.sexLabel = sexLabel;
    }

    public String getApplyStatusLabel() {
        return applyStatusLabel;
    }

    public void setApplyStatusLabel(String applyStatusLabel) {
        this.applyStatusLabel = applyStatusLabel;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getParentCompanyName() {
        return parentCompanyName;
    }

    public void setParentCompanyName(String parentCompanyName) {
        this.parentCompanyName = parentCompanyName;
    }

    public String getBranchCompanyName() {
        return branchCompanyName;
    }

    public void setBranchCompanyName(String branchCompanyName) {
        this.branchCompanyName = branchCompanyName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
package com.sc.common.service;


import com.sc.common.dto.WebResponseDto;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.entity.Example;
import java.util.List;

public interface BaseService<T> {
    List<?> selectByPageNumSize(Object search, int pageNum, int pageSize);

    T selectByPrimaryKey(Object key);

    boolean existsWithPrimaryKey(Object key);

    T selectOne(T search);

    T selectOneByExample(Example example);

    List<T> select(T search);

    List<T> selectByExample(Example example);

    List<T> selectAll();

    int selectCount(T search);

    int selectCountByExample(Example example);

    List<T> selectByRowBounds(T var1, RowBounds var2);

    List<T> selectByExampleAndRowBounds(T var1, RowBounds var2);

    int insert(T entity);

    int insertSelective(T entity);

    int insertList(List<T> entities);

    int updateByPrimaryKey(T entity);

    int updateByPrimaryKeySelective(T entity);

    int updateByExample(T var1,Example example);

    int updateByExampleSelective(T var1,Example var2);

    int deleteByPrimaryKey(Long key);

    int deleteByExample(Object example);

    /**
     * 自定义方法，批量更新
     * @param entities
     * @return
     */
    int batchUpdate(List<T> entities);


    /**
     * 自定义方法，批量删除
     * @param keys
     * @return
     */
    int batchDelete(List<?> keys);


    /**
     * 自定义方法，复杂业务可重写此方法，在controller调用此方法
     */
    WebResponseDto create(Object obj);

    /**
     * 自定义方法，复杂业务可重写此方法，在controller调用此方法
     */
    WebResponseDto update(Object obj);

    /**
     * 自定义方法，复杂业务可重写此方法，在controller调用此方法
     */
    WebResponseDto delete(Object obj);
}

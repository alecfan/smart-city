package com.sc.common.cache;


import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.util.SpringContextHolder;
import org.springframework.aop.support.AopUtils;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * 缓存对象工厂，需要实现缓存的持久化对象与缓存业务对象实例的映射
 */
@Component
public class CacheFactory {
    public CacheAbstract getCacheBo(String source) {
        Object object = SpringContextHolder.getApplicationContext().getBeansWithAnnotation(EnableComplexCaching.class);
        if (object != null) {
            Map<String, Object> dataServiceMap = (Map<String, Object>) object;
            Set<Map.Entry<String, Object>> entrySet = dataServiceMap.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                if (entry == null || entry.getValue() == null) {
                    continue;
                }

                EnableComplexCaching enableComplexCaching = AopUtils.getTargetClass(entry.getValue()).getAnnotation(EnableComplexCaching.class);
                if(enableComplexCaching != null){
                    if(source.equals(enableComplexCaching.dependsOnPOSimpleName())){
                        CacheAbstract cacheAbstract = (CacheAbstract) entry.getValue();
                        return cacheAbstract;
                    }
                }
            }
        }
        return null;
    }
}

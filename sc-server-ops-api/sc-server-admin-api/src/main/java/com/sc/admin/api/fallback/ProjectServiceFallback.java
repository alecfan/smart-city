package com.sc.admin.api.fallback;

import com.sc.admin.api.ProjectService;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.project.SysProject;
import org.springframework.stereotype.Component;

@Component
public class ProjectServiceFallback implements ProjectService {
    @Override
    public WebResponseDto select(String ctx, SysProject search) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }
}
